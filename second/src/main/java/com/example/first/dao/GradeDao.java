package com.example.first.dao;

import com.example.first.entity.Grade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface GradeDao extends JpaRepository<Grade,Long>, JpaSpecificationExecutor<Grade> {
}
