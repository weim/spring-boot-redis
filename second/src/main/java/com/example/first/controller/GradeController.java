package com.example.first.controller;

import com.example.first.service.GradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class GradeController {


    @Autowired
    private GradeService gradeService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String test(HttpServletRequest request) {

        gradeService.find();

        return "hello group";
    }

}
