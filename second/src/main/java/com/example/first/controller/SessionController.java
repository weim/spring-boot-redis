package com.example.first.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class SessionController {

    @RequestMapping("/second/v1")
    public void print(String name, HttpServletRequest request) {

        request.getSession().setAttribute("name",name);

        System.out.println("/second/v1==>"+name);

    }


    @RequestMapping("/second/v2")
    public void print2(HttpServletRequest request) {

        String name = request.getSession().getAttribute("name").toString();

        System.out.println("/second/v2==>"+name);

    }

    @RequestMapping("/second/v3")
    public void print3(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String url = request.getParameter("url2");
        System.out.println("/second/v3==>"+url);
        response.sendRedirect(url);
    }

}
