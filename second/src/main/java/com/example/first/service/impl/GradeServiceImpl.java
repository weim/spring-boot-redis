package com.example.first.service.impl;

import com.example.first.entity.User;
import com.example.first.service.GradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GradeServiceImpl implements GradeService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public String find() {

        User user = (User) redisTemplate.opsForValue().get("user");
        System.out.println("GroupServiceImpl==>"+user.name);

        String test = stringRedisTemplate.opsForValue().get("string");
        System.out.println("test===>"+test);

        List<User> users = (List<User>) redisTemplate.opsForValue().get("users");
        System.out.println("GroupServiceImpl2==>"+users.size());


        return "group_service";

    }
}
