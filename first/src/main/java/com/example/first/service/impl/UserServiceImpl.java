package com.example.first.service.impl;

import com.example.first.dao.UserDao;
import com.example.first.entity.User;
import com.example.first.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {


    @Autowired
    private UserDao userDao;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public String find(String test) {

        redisTemplate.opsForValue().set("ccc",test);
        System.out.println("redisTemplate==>"+redisTemplate.opsForValue().get("ccc"));

        stringRedisTemplate.opsForValue().set("ddd",test+"2");
        System.out.println("redisTemplate2==>"+stringRedisTemplate.opsForValue().get("ddd"));
        return "test_user_service";
    }

    @Override
    public User addUser(User user) {
        user = userDao.save(user);
        redisTemplate.opsForValue().set("user",user);
        stringRedisTemplate.opsForValue().set("string",user.toString());
        return user;
    }

    @Override
    public List<User> findAll() {
        List<User> ret =  userDao.findAll();
        redisTemplate.opsForValue().set("users",ret);
        return ret;
    }
}
