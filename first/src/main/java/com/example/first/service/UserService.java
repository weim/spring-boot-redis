package com.example.first.service;

import com.example.first.entity.User;

import java.util.List;

public interface UserService {
    String find(String test);

    User addUser(User user);

    List<User> findAll();
}
