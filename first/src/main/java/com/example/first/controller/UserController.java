package com.example.first.controller;

import com.example.first.entity.User;
import com.example.first.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String test() {

        return "hello user";
    }

    @RequestMapping(value = "/add/{userId}", method = RequestMethod.GET)
    public User addUser(@PathVariable Long userId) {

        User user = new User();
        user.id = userId;
        user.name = "user_"+userId;
        user.GradeId = userId;

        user = userService.addUser(user);

        return user;
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public List<User> getUsers(HttpServletRequest request) {

        List<User> userList = userService.findAll();
        return userList;
    }



}
