package com.example.first.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * http://127.0.0.1:8088/first/v1?name=aaa
 * http://127.0.0.1:8088/first/v2
 * http://127.0.0.1:8088/first/v3?name3=111&url=http://127.0.0.1:8089/second/v3?url2=http://127.0.0.1:8088/first/v4


 * http://127.0.0.1:8088/first/v1?name=bbb
 * http://127.0.0.1:8088/first/v2
 * http://127.0.0.1:8088/first/v3?name3=222&url=http://127.0.0.1:8089/second/v3?url2=http://127.0.0.1:8088/first/v4


 * http://127.0.0.1:8088/first/v1?name=ccc
 * http://127.0.0.1:8088/first/v2
 * http://127.0.0.1:8088/first/v3?name3=333&url=http://127.0.0.1:8089/second/v3?url2=http://127.0.0.1:8088/first/v4


 * http://127.0.0.1:8088/first/v1?name=ddd
 * http://127.0.0.1:8088/first/v2
 * http://127.0.0.1:8088/first/v3?name3=444&url=http://127.0.0.1:8089/second/v3?url2=http://127.0.0.1:8088/first/v4
 */


@RestController
public class SessionController {

    @RequestMapping("/first/v1")
    public void print(String name, HttpServletRequest request) {

        request.getSession().setAttribute("name",name);

        System.out.println("/first/v1==>"+name);

    }


    @RequestMapping("/first/v2")
    public void print2(HttpServletRequest request) {

        String name = request.getSession().getAttribute("name").toString();

        System.out.println("/first/v2==>"+name);

    }

    @RequestMapping("/first/v3")
    public void print3(String name3,String url,HttpServletRequest request, HttpServletResponse response) throws IOException {

        request.getSession().setAttribute("name3",name3);

        System.out.println("/first/v3==>"+name3);

        response.sendRedirect(url);
    }

    @RequestMapping("/first/v4")
    public void print4(HttpServletRequest request, HttpServletResponse response) throws IOException {


        String name3 = request.getSession().getAttribute("name3").toString();

        System.out.println("/first/v4==>"+name3);
    }

}
